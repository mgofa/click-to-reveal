jQuery( document ).ready( function( $ ) {
    $( ".wrapper" ).click( function() { 
        $( this ).find( "svg > image" ).css( "filter", "" );
        $( this ).find( "p" ).css( "display", "none" );
	if( $( this ).find( ".overlay" ) ) {
		var $overlay = $( this ).find( ".overlay" );
		$overlay.css( {
			backgroundColor: 'transparent',
			backgroundImage: 'url(' + $( this ).find( 'image' ).attr( 'xlink:href' ) + ')'
		} );
	}
    } );
    if( Modernizr && !Modernizr.svgfilters ) {
        $( '.wrapper' ).each( function() {
            $( this ).append( '<div class="overlay"></div></div>' );
        } );
    }
} );
