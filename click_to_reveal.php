<?php
/*
Plugin Name: Click-to-Reveal
Version: 2.0
Author: Chris Lewis
Description: Adds shortcode for blurring images until clicked by user.
*/

function reveal_scripts() {
    wp_enqueue_style( 'clicktoreveal-stylesheet', plugin_dir_url( __FILE__ ) . "click.css" );
    wp_enqueue_script( 'clicktoreveal-script', plugin_dir_url( __FILE__ ) . 'click.js', array( 'jquery', 'modernizr' ), '', true );
    wp_enqueue_script( 'modernizr' );
}
add_action( 'wp_enqueue_scripts', 'reveal_scripts' );

function reveal_shortcode( $atts, $content = null ) {
    $options = shortcode_atts( array(
        'url' => '',
        'width' => '400',
        'height' => '600'
    ), $atts );
    
    $unique_id = md5( $options[ "url" ] );

    $template = <<<HTML
<div class="wrapper" style="width: %spx; height: %spx;">
    <svg style="width: %spx; height: %spx;">
        <image x="0" y="0" width="100%%" height="%s" xlink:href="%s" style="filter: url(#%s)"></image>
        <filter id="%s">
            <feGaussianBlur stdDeviation="20"></feGaussianBlur>
        </filter>
    </svg>
    <p style="top: %spx">Click to reveal</p>
</div>
HTML;

    return sprintf( $template, $options[ "width" ], $options[ "height" ], $options[ "width" ], $options[ "height" ], $options[ "height" ], $options[ "url" ], $unique_id, $unique_id, -1 * intval( $options[ "height" ] ) / 2 - 45 );
}
add_shortcode( 'click_to_reveal', 'reveal_shortcode' );

?>
